import numpy as np
from tensorflow.python.keras.datasets import mnist
from tensorflow.python.keras.utils import to_categorical
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Dense
from tensorflow.python.keras.regularizers import l2
from tensorflow.python.keras.callbacks import EarlyStopping
from tensorflow.python.keras.wrappers.scikit_learn import KerasClassifier
from tensorflow.python.keras.callbacks import Callback
from sklearn.model_selection import KFold, GridSearchCV, RandomizedSearchCV
from sklearn.metrics import accuracy_score, confusion_matrix


seed = 7
input_dim = 28*28
(x_train, y_train), (x_test, y_test) = mnist.load_data()
es = EarlyStopping(monitor='val_loss', min_delta=10e-10, patience=10, verbose=1)


class Metrics(Callback):
    def on_train_begin(self, logs={}):
        self._ppd = []

    def on_epoch_end(self, epoch, logs={}):
        for val_d in self.validation_data[0]:
            self._ppd.append(point_plane_dist(self.model.get_weights()[0], val_d, self.model.get_weights()[1]))


def select_classes(a=3, b=7):
    global input_dim
    global x_train
    global y_train
    global x_test
    global y_test

    x_train = x_train[(y_train == a) | (y_train == b)] / 255.0
    x_test = x_test[(y_test == a) | (y_test == b)] / 255.0
    y_train = y_train[(y_train == a) | (y_train == b)]
    y_test = y_test[(y_test == a) | (y_test == b)]

    x_train = x_train.reshape((len(y_train), input_dim))
    x_test = x_test.reshape((len(y_test)), input_dim)

    y_train[y_train == a] = 0
    y_train[y_train == b] = 1
    y_test[y_test == a] = 0
    y_test[y_test == b] = 1

    y_train = to_categorical(y_train)
    y_test = to_categorical(y_test)


def create_model(penalty=0.0):
    model = Sequential()
    model.add(Dense(2, input_dim=input_dim,
                    activation='softmax', activity_regularizer=l2(penalty) if penalty != 0 else None))
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    return model


def hp_optimization(t):
    global seed
    np.random.seed(seed)
    classifier = KerasClassifier(build_fn=create_model, epochs=8, batch_size=4, verbose=1)
    penalty = np.linspace(0.22, 0.26, 10)
    params = dict(penalty=penalty)

    if t == 'g':
        search = GridSearchCV(estimator=classifier, param_grid=params, n_jobs=1)
    else:
        search = RandomizedSearchCV(estimator=classifier, param_distributions=params, n_jobs=1)

    results = search.fit(x_train, y_train, validation_data=(x_train, y_train), callbacks=[es])

    print("Best: %f using %s" % (results.best_score_, results.best_params_))
    means = results.cv_results_['mean_test_score']
    stds = results.cv_results_['std_test_score']
    params = results.cv_results_['params']
    for mean, stdev, param in zip(means, stds, params):
        print("%f (%f) with: %r" % (mean, stdev, param))


def kfoldver(k):
    kfold = KFold(n_splits=k, shuffle=True, random_state=seed)
    cvscores = []
    for train, test in kfold.split(x_train):
        model = create_model()
        model.fit(x_train[train], y_train[train], epochs=8, batch_size=4, verbose=1)
        scores = model.evaluate(x_train[test], y_train[test], verbose=1)
        cvscores.append(scores[1] * 100)
    print("%.2f%% (+/- %.2f%%)" % (np.mean(cvscores), np.std(cvscores)))


def point_plane_dist(v, x0, d):
    s = abs(sum([vi*xi for vi, xi in zip(v, x0)]) + d)
    e = np.sqrt(sum([vi**2 for vi in v]))
    return s / e


def ppd_test_score(model):
    global input_dim
    global x_test
    ppt1 = 0
    ppt2 = 0
    for x in x_test:
        ppt1 += point_plane_dist(model.get_weights()[0][:, 0], x, model.get_weights()[1][0])
        ppt2 += point_plane_dist(model.get_weights()[0][:, 1], x, model.get_weights()[1][1])
    avg_ppt1 = ppt1 / input_dim
    avg_ppt2 = ppt2 / input_dim
    return avg_ppt1, avg_ppt2


def main():
    select_classes()
    #hp_optimization('r')
    model = create_model(0.24)
    model.fit(x_train, y_train, epochs=8, batch_size=4,
              validation_data=(x_train, y_train),callbacks=[es], verbose=1)

    y_pred = model.predict(x_test)
    conf_matrix = confusion_matrix(y_test.argmax(axis=1), y_pred.argmax(axis=1))

    print(conf_matrix)
    print(accuracy_score(y_test.argmax(axis=1), y_pred.argmax(axis=1)) * 100)
    print(ppd_test_score(model))


if __name__ == '__main__':
    main()
